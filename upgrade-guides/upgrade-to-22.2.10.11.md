# To upgrade from ecloud 21.x.x.x to 22.2.10.11

- As usual, upgrade your OS with latest patchs, optionally take backup/snapshot

  - NB: you may want to filter out incomming email (TCP 25 & 587) during this upgrade, to avoid losing any messages in case of a rollback

- In your `docker-compose.yml` file update the following:
  - Set the nextcloud image to `registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:22-2-10-11`
  - Set the mailserver image to `mailserver2/mailserver:1.1.10`
- Run `docker-compose pull`
- Run `docker-compose up -d`

- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`

- Run `docker-compose exec -T --user www-data nextcloud php occ db:add-missing-indices`
