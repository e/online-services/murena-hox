# To upgrade from ecloud 22.x.x.x to 23.0.11.22

- As usual, upgrade your OS with latest patchs, optionally take backup/snapshot
  - NB: you may want to filter out incomming email (TCP 25 & 587) during this upgrade, to avoid losing any messages in case of a rollback

- Go to `/mnt/repo_base`, then run:
  - `docker-compose stop`
  - `git pull origin master`

- In your `docker-compose.yml` file update the following:
  - Set the nextcloud image to `registry.gitlab.e.foundation/e/infra/ecloud/nextcloud/selfhost:23.0.11.22`
  - Set the mailserver image to `mailserver2/mailserver:1.1.11`
- Run `docker-compose pull`
- If pulls are OK, run `docker-compose up -d`

- Examine `docker-compose logs --tail=500 nextcloud` for the following messages:
  - `nextcloud       | Upgrading nextcloud from x.x.x.x ...` (x.x.x.x is your previous Nextcloud version)
  - `nextcloud       | Update successful`

- Get your country code (first column) from https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements
- Run:
  - `docker-compose exec -T --user www-data nextcloud php occ config:system:set defaultapp --value="ecloud-dashboard"`
  - `docker-compose exec -T --user www-data nextcloud php occ db:add-missing-indices`
  - `docker-compose exec -T --user www-data nextcloud php occ config:system:set default_phone_region --value="xx"` replacing xx with your country code (ie: FR)

- To set the new and improved dashboard as your default app, run:
  - `docker-compose exec -T --user www-data nextcloud php occ app:enable ecloud-dashboard`
