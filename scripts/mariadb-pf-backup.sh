#!/usr/bin/env bash
#==============================================================================
#title         :mariadb-pf-backup.sh
#description   :This script will backup /e/ Postfix database from MariaDB Docker container
#author        :sylvain@murena.io
#date          :20221120
#version       :1.0
#dependances   :selfhosted /e/ Cloud, pigz (option)
#usage         :- shell : bash /mnt/repo-base/scripts/mariadb-pf-backup.sh
#usage         :- crontab : 30 4 * * * bash /mnt/repo-base/scripts/mariadb-pf-backup.sh >> /var/log/mariadb-pf-backup.log 2>&1
#usage         :- systemd :
#usage         :-- Service unit (/etc/systemd/system/mariadb-pf-backup.service) :
#usage         :    [Unit]
#usage         :    Description=Perform Postfix MariaDB backup
#usage         :    [Service]
#usage         :    Type=simple
#usage         :    Nice=19
#usage         :    IOSchedulingClass=2
#usage         :    IOSchedulingPriority=7
#usage         :    ExecStart=/mnt/repo-base/scripts/mariadb-pf-backup.sh
#usage	       :-- Timer unit (/etc/systemd/system/mariadb-pf-backup.timer) :
#usage         :    [Unit]
#usage         :    Description=Backup Postfix database
#usage         :    [Timer]
#usage         :    OnCalendar=*-*-* 04:30:00
#usage         :    [Install]
#usage         :    WantedBy=timers.target
#usage         :-- commands :
#usage         :   systemctl start mariadb-pf-backup.timer
#usage         :   systemctl enable mariadb-pf-backup.timer
#usage         :   systemctl list-timers
#notes         :https://gitlab.e.foundation/e/backlog/-/issues/4027
#==============================================================================
set -e

# Get all /e/ Cloud variables
source "/mnt/repo-base/scripts/base.sh"

# Get custom variables, if any
if [[ -x "/mnt/repo-base/scripts/custom.sh" ]]; then
  source "/mnt/repo-base/scripts/custom.sh"
fi

# !! EDIT THIS LINES FOR YOUR USAGE !!

# Vars are from base.sh script
MARIA_DB=$PFDB_DB
KEEP=2
BACKUPS_PATH="/mnt/repo-base/backups/SQL"

# Uncomment this if you run manually or from crontab
# Useless with systemd, as outputs will go to journal
#shopt -s expand_aliases
#alias echo='echo $(date --rfc-3339=ns)'

# !! PLEASE DO NOT EDIT PAST THIS LINE !!

# Get current timestamp for backup file naming
NOW=$(date +"%Y-%m-%d_%s")
# This is Docker container name
MARIA_SERVICE="mariadb"
# Full backup file name with path
BACKUP_FILE="${BACKUPS_PATH}/${NOW}_${MARIA_DB}.gz"

# Check if pigz available, fallback to gzip
if [[ $(command -v pigz) ]]; then
  COMPCMD="pigz"
else
  COMPCMD="gzip"
fi

# Easier to run Docker commands from here
cd "/mnt/repo-base"

# Test if MariaDB container/service is online
if [[ -z $(docker-compose ps --services --filter "status=running" | grep -i $MARIA_SERVICE) ]]; then
  echo "KO mariadb Docker service not running, abort"
  exit 1
else
  echo "OK mariadb Docker service is running"
fi

# If OK, continue with creating target dir if necessary
mkdir -p $BACKUPS_PATH 2>/dev/null

# Set SECONDS timer to zero, to quantify runtime
SECONDS=0
# Then start backup
# Note : MARIADB_BACKUP_* are from base.sh script !
docker-compose exec -T $MARIA_SERVICE mysqldump --opt --single-transaction -u$MARIADB_BACKUP_USER -p$MARIADB_BACKUP_PASSWORD $MARIA_DB |$COMPCMD >$BACKUP_FILE
# As we can't easily get the errors from previous command, easier to check the message in backup file ;)
BACKUP_RESULT=$($COMPCMD -d -c $BACKUP_FILE |tail -1 |grep -i "dump completed")
if [[ -z $BACKUP_RESULT ]]; then
  echo "KO bad backup, please see output file for messages"
  exit 1
else
  echo "OK backup terminated successfully"
# Check execution time, advertize pigz if relevant
  if [[ ($COMPCMD == "gzip") && ($SECONDS -gt 300) ]]; then
    echo "WARNING backup took more than 5mn, you may give pigz a try"
  fi
# If backup successful, purge the old files (NB : command outputted to log)
  echo "Deleting old backups"
  find $BACKUPS_PATH -type f -iname "*_${MARIA_DB}.gz" |sort -n |head -n -$KEEP |xargs -t rm -f
fi

# End of script
